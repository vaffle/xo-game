fun main() {
    while (true){
        try {
            print("Please Input row col : ")
            val input = readLine()
            val numStr = input?.split(" ")
            if(numStr?.size?:0 == 2){
                val row = numStr?.get(0)?.toInt()
                val col = numStr?.get(1)?.toInt()
                println("$row , $col")
                break
            } else {
                println("Please input row, col !!!!!")
            }
        }catch (e:NumberFormatException){
            println("Please input row, col in number format !!!!!!")
        }
    }
}